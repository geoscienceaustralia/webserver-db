# Overview

![Architecture Diagram]
(./img/architecture.png)

This will create:

 * An autoscaling web server group
 * An elastic load-balancer
 * A Postgres database
 * A VPC with a NAT gateway

## Preperation

1. Install Pre-requisites
    1. Download [AWSCLI](https://aws.amazon.com/cli/)
    2. Download [Terraform 0.10](https://www.terraform.io/downloads.html)

2. Create a service user to run the Terraform Scripts
    1. Sign into the AWS Console
    2. Click Services
    3. Under `Security, Identity and Compliance` Select IAM
    4. Select Users
    5. Click Add user
    6. Give the account a username in the format: svcTF<appname>
    7. Select Programmatic access
    8. Click Next:Permissions
    9. Select: `Attach existing Policies Directly`
    10. Click Create Policy (it will open in a new tab)
    11. Next to Create Your Own Policy, Click `Select`
    12. Set the name to be `<appname>TerraformRunner`
    13. Copy the policy from the policies folder and paste it in the `Policy Document` field
    14. Click Create Policy
    15. Change tabs back to the User creation Tab
    16. In the AWS Console click Refresh (not the browser refresh button)
    17. Search for your new policy by it's name
    18. Click the checkbox on the left of the policy
    19. Click Next:Review (down the bottom)
    20. Click create user
    21. Copy the `access key id` and `secret access key`
    22. For a dev machine run `aws configure`
    23. For a bitbucket pipeline set the environment variables `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`

3. Create a new s3 bucket (this is used to store the current state of your infrastructure):
    1. Login to the AWS Console
    2. Click Services in the top Right
    3. Select S3 Under Storage
    4. Click Create Bucket
    5. Set the Bucket Name ( We usually use <accountname>-tfstate but it's up to your discretion)
    6. Select Sydney from the region dropdown
    7. Click Create

4. Configure Terraform to store it's state in the bucket you just created
    
```
terraform {
    required_version = ">= 0.9.1"
    backend "s3" {
        bucket = "INSERT_YOUR_BUCKET_NAME_HERE"
        key = "three-tier-dev/"
        region = "ap-southeast-2"
        dynamodb_table = "terraform"
        encrypt = true
    }
}
```

5. Create a DynamoDB table in AWS:
    1. Login to the AWS Console
    2. Ensure that your active region is "Sydney"; or aligned with the region you'll be using
    3. Click Services in the top Right
    4. Under Databases, select DynamoDB
    5. Click Create Table
    6. Set Table Name to be terraform
    7. Set Primary Key to be LockID
    8. Leave all other options and select Create

6. Verify Amazon Machine Images (AMIs) are available:
    1. Check that your aws space has the required Amazon Machine Images (AMIs)
        1. Login to the AWS Console
        2. Ensure that your active region is "Sydney"; or aligned with the region you'll be using
        3. Click Services in the top Right
        4. Under Compute select EC2
        5. On the right Hand Menu, under IMAGES select AMIs
        6. Check there are AMIs in your aws space with the tags:
            1. version: dev, application: Jumpbox
            2. version: dev, application: simplewww-test
        7. If either of these images are missing please create these images using packer or from base images.
    2. Create the AMI using Packer
        1. Create and upload the simplewww-test AMI by following this
           [tutorial](https://bitbucket.org/geoscienceaustralia/simple-webserver
    3. Create the AMIs from base Images
        1. Find a machine image id of an existing image; if you don't have one prepared you can use the 
            Amazon Linux AMI
            1. To find an Amazon Linux image id
            2. Click Services in the top Right
            3. Under Compute click EC2
            4. In the main Pain find Launch Instance (Blue button) under Create Instance and Click
            5. It is in the format "ami-xxxxxxxx" to the right of the Amazon Linux AMI title in bold
            6. Copy this code
        2. Click Services in the top Right
        3. Under Compute click EC2
        4. Under IMAGES click AMIs in the far left column
        5. To the left of the search bar there is a toggle; select "Public images" if using Amazon Linux AMI
        6. Paste the image id into the search bar and hit enter
        7. If using an Amazon Linux AMI
            1. Right click on the first result and hit "Copy AMI"
            2. In the Destination region input Asia Pacific (Syndey)
            3. click Copy AMI
            4. Change the filter to the left of the search bar to "Owned by me" and clear the image id filter
        8. Select your image from the results
        9. Select the Tags tab from the image information in the lower part of the screen
        10. Click Add/Edit Tags
            1. Type "application" into the Key field and the name of the application tag into the Value field
            2. Click "Create Tag"
            3. Type "version" into the Key field and "dev" into the Value field
            4. Click Save
        11. If required process to create an image for application "Jumpbox" and application "simplewww-test"


Now if you run terraform init you will see :

```
Initializing the backend...

Backend configuration changed!
```


## Set Variables

Set the following variables in _variables.tf:

 *Terraform Remote State*
 * bucket - an s3 bucket that exists in your aws space (see Preperation)
 * dynamodb_table - a dynamodb table in your aws space with the primary key LockID (see Preperation)

 *SSH Keys*
 * key_name - the ssh key to be added to your EC2 instances
 
Set the following environment variables (substituting the details of your application)
 
 * `export TF_VAR_db_admin_username=<adminusername>`
 * `export TF_VAR_db_admin_password=<adminpassword>`
 * `export TF_VAR_db_user_username=<applicationname>`
 * `export TF_VAR_db_user_password=<userpassword>`
 * `export TF_VAR_ssh_ip_address=<yourip>/32`

For Example:
 * `export TF_VAR_db_admin_username=dbadmin`

## Creating your infrastructure

1. `terraform init`
2. `terraform get`
3. `terraform plan`
4. `terraform apply`

This command will output your database endpoint, which you will need below.

## Destroying your infrastructure

1. `terraform destroy`

This is assuming that you ran `terraform init` previously on the same machine
This command will tear down everything that terraform created.


## Connecting to database

From one of the application servers, you can connect to the RDS by running:

`mysql -h <endpointname> -u <username> -p`

You will be prompted to enter your password
you can then run: `use <database>` to allow you to query the database.

For example:
`mysql -h database.three-tier -u dbadmin -p`

`use application`

# SSH connection to your application server

Please note that jump boxes or jump hosts have been intentionally removed from the Autobots sample Terraform projects. Instead, we are now suggesting that people use AWS Systems Manager (SSM) Session Manager in order to connect to their application server via SSH. This has been done to improve security, be more cost efficient and reduce maintenance. Removing the need for jump boxes means no more risk of having open security groups, no more extra cost, patching and maintenance of an extra EC2 instance.

## AWS Systems Manager (SSM) Session Manager prerequisites

In order to use AWS Session Manager with your EC2 instance you simply need to ensure 2 things:

1. SSM Agent is installed on your instance.
2. The SSM policy or sufficient SSM permissions have been added to your EC2 instance profile.

The first point has been made easier for some users as the SSM Agent now comes with the following AMIs:

1. Windows Server 2003-2012 R2 AMIs published in November 2016 or later
2. Windows Server 2016 and 2019
3. Amazon Linux • Amazon Linux 2
4. Ubuntu Server 16.04
5. Ubuntu Server 18.04

If you are not using one of the above AMI's then a manual installation of SSM agent has to be performed. The following AWS documentation describes how to do this with Windows & Linux(Flavours):
[https://docs.aws.amazon.com/systems-manager/latest/userguide/systems-manager-ug.pdf#systems-manager-quick-setup](https://docs.aws.amazon.com/systems-manager/latest/userguide/systems-manager-ug.pdf#systems-manager-quick-setup)

Finally, in order to add the necessary SSM permissions to your EC2 instance profile you can use AWS SSM Quick Setup. You can choose to have Quick Setup create and configure these roles for you by choosing “Use the default role”. This should take care of getting all the instances under managed resources:
[https://docs.aws.amazon.com/systems-manager/latest/userguide/systems-manager-quick-setup.html](https://docs.aws.amazon.com/systems-manager/latest/userguide/systems-manager-quick-setup.html)

# Enabling HTTPS

HTTPS is not configured by default to lower the barrier to entry.
Enabling it is simple. 

1. Follow the [AWS Documentation](http://docs.aws.amazon.com/acm/latest/userguide/gs-acm-request.html) to request a certificate 
2. Edit modules/public_layer/elb.tf
3. Uncomment the `aws_acm_certificate` block
4. Add your domain to the `aws_acm_certificate` block
5. Uncomment the `HTTPS Listener` block
6. View the change using `terraform plan`
7. Deploy the change using `terraform apply`

_N.B._ You should consider adding a https redirect to your server so users accessing the site via HTTP will be redirected to HTTPS.
