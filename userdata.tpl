
export MYSQL_ROOT_USER=${db_admin_username}
export MYSQL_ROOT_PWD=${db_admin_password}
export DATABASE_USER=${db_user_username}
export DATABASE_USER_PWD=${db_user_password}
export DATABASE_NAME=${db_database_name}
export RDS_ENDPOINT=${db_endpoint}
export DB_PORT=${db_port}


#--------------------------------------------------------------
# Configure Database
#--------------------------------------------------------------

# The database is only configured the first time
# the stack is created. 

# Test if database already exists
RESULT=`mysqlshow --user=$MYSQL_ROOT_USER --password=$MYSQL_ROOT_PWD --host=$RDS_ENDPOINT --port=$DB_PORT $DATABASE_NAME 2>/dev/null | grep Database | awk '{print $2;}'`
if [ "$RESULT" == $DATABASE_NAME ]; then
    echo 'Database already exists - skipping creation...'    
else
    # Create app database
    echo 'Creating database...'
    mysql -u$MYSQL_ROOT_USER -p$MYSQL_ROOT_PWD -h$RDS_ENDPOINT -P$DB_PORT -e "CREATE DATABASE $DATABASE_NAME;"

    # Clean up environment variable interpolation - create a temporary file for the GRANT SQL command
    echo "GRANT ALL ON DATABASE_NAME.* to 'DATABASE_USER' IDENTIFIED BY 'DATABASE_USER_PWD';" > ~/grant.sql
    sed -i "s/DATABASE_NAME/$DATABASE_NAME/" ~/grant.sql
    sed -i "s/DATABASE_USER/$DATABASE_USER/" ~/grant.sql
    sed -i "s/DATABASE_USER_PWD/$DATABASE_USER_PWD/" ~/grant.sql
    mysql -u$MYSQL_ROOT_USER -p$MYSQL_ROOT_PWD -h$RDS_ENDPOINT -P$DB_PORT < ~/grant.sql
    rm ~/grant.sql

    # Flush privileges
    mysql -u$MYSQL_ROOT_USER -p$MYSQL_ROOT_PWD -h$RDS_ENDPOINT -P$DB_PORT -e "FLUSH PRIVILEGES;"

fi 

#--------------------------------------------------------------
# Cleanup Environment Variables
#--------------------------------------------------------------

# remove variables
unset MYSQL_ROOT_USER
unset MYSQL_ROOT_PWD
unset DATABASE_USER
unset DATABASE_USER_PWD
unset DATABASE_NAME
unset RDS_ENDPOINT
unset DB_PORT
