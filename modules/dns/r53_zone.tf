#==============================================================
# DNS / r53_zone.tf
#==============================================================

# Create a DNS zone to host our DNS entries

resource "aws_route53_zone" "zone" {
  name = var.zone

  vpc {
    vpc_id = var.vpc_id
  }

  tags = {
    name        = "${var.stack_name}-${var.workplace}-r53-zone"
    owner       = var.owner
    stack_name  = var.stack_name
    workplace   = var.workplace
    terraform   = "true"
  }
}
