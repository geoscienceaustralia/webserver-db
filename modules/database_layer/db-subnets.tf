#==============================================================
# Database / outputs.tf
#==============================================================

# Create a subnet in each availability zone.

resource "aws_subnet" "database" {
  count  = length(var.availability_zones)
  vpc_id = var.vpc_id

  cidr_block        = element(var.database_subnet_cidr, count.index)
  availability_zone = lookup(var.availability_zones, count.index)

  tags = {
    Name        = "${var.stack_name}-database-subnet-${var.workplace}-${lookup(var.availability_zones, count.index)}"
    owner       = var.owner
    stack_name  = var.stack_name
    workplace   = var.workplace
    created_by  = "terraform"
  }
}
