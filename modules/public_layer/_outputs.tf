#==============================================================
# Public / outputs.tf
#==============================================================

#--------------------------------------------------------------
# Network
#--------------------------------------------------------------

output "public_subnet_ids" {
  value = aws_subnet.public.*.id
}

output "ngw_ids" {
  value = aws_nat_gateway.nat.*.id
}

#--------------------------------------------------------------
# Elastic Load Balancer
#--------------------------------------------------------------

output "elb_sg_id" {
  value = aws_security_group.elb_sg.id
}

output "elb_name" {
  value = aws_elb.elb.name
}

output "elb_dns_name" {
  value = aws_elb.elb.dns_name
}

output "elb_dns_hosted_zone" {
  value = aws_elb.elb.zone_id
}
