#==============================================================
# Public / elb-sg.tf
#==============================================================

# Security groups for Elastic Load Balancer

resource "aws_security_group" "elb_sg" {
  # Allow HTTP from anywhere
  name = "elb_sg"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [var.http_ip_address[var.workplace]]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [var.http_ip_address[var.workplace]]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = var.vpc_id

  tags = {
    Name        = "${var.stack_name}-${var.workplace}-elb-http-inbound"
    owner       = var.owner
    stack_name  = var.stack_name
    workplace   = var.workplace
    created_by  = "terraform"
  }
}
